const path = require('path')
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router(path.join(__dirname, 'db.json'));
const middlewares = jsonServer.defaults()

server.use(middlewares)
server.use(jsonServer.bodyParser)

const getResourcesPage = (params, collection) => {
    const items = router.db.get(collection).value()
    const {page, pageSize} = params;
    if (!page || !pageSize) {
        return {error: 500}
    }
    const firstIndex = page * pageSize;
    const secondIndex = firstIndex + (+ pageSize);
    return {
        fullSize: items.length,
        items: items.slice(firstIndex, secondIndex)
    }
};

server.post("/reservation-save", (req, res) => {
    const id = "R" + router.db.get("reservations").value().length
    router.db.get("reservations").push({
        id,
        ...req.body
    }).write()
    res.status(200).send()
})
server.get("/reservations-page/:page/:pageSize", (req, res) => {
    const result = getResourcesPage(req.params, "reservations");
    if ('error' in result) {
        res.status(500).jsonp({error: "Intern error"})
        return
    }
    res.jsonp(result)
})

server.get("/foodtrucks-page/:page/:pageSize", (req, res) => {
    const result = getResourcesPage(req.params, "foodtrucks");
    if ('error' in result) {
        res.status(500).jsonp({error: "Intern error"})
        return
    }
    res.jsonp(result)
})

server.get("/trucks-names", (req, res) => {
    const items = router.db.get("foodtrucks").value()
    res.jsonp(items.map(item => ({name: item.name, id: item.id})))
})

server.post("/foodtrucks-save", (req, res) => {
    const id = "T" + router.db.get("foodtrucks").value().length
    router.db.get("foodtrucks").push({
        id,
        ...req.body
    }).write()
    res.status(200).send()
})

server.use(router)

server.listen(3000, () => {
    console.log('JSON Server is running')
})
