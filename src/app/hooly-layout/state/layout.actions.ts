import { MatDrawerMode } from '@angular/material/sidenav';
import { createAction, props } from '@ngrx/store';

export const LayoutActions = {
  toggleSideMenu: createAction('[Layout] Display Side Menu'),
  setSideMenuOpened: createAction(
    '[Layout] Open Side Menu',
    props<{ open: boolean }>()
  ),
  setMenuMode: createAction(
    '[Layout] Set Menu Mode',
    props<{ mode: MatDrawerMode }>()
  ),
  closeMenuOnClickLink: createAction('[Layout] Close menu on click link'),
};
