import { createFeatureSelector, createSelector } from '@ngrx/store';
import { layoutFeatureKey, LayoutState } from './layout.reducer';
const selectState = createFeatureSelector<LayoutState>(layoutFeatureKey);
const selectIsSideMenuOpened = createSelector(
  selectState,
  (state) => state.isSideMenuOpen
);
const selectMenuOpenMode = createSelector(
  selectState,
  (state) => state.menuMode
);
export const LayoutSelectors = {
  selectState,
  selectIsSideMenuOpened,
  selectMenuOpenMode,
};
