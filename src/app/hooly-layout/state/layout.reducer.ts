import { MatDrawerMode } from '@angular/material/sidenav';
import { createReducer, on } from '@ngrx/store';
import { LayoutActions } from './layout.actions';

export const layoutFeatureKey = 'layout feature key';
export interface LayoutState {
  isSideMenuOpen: boolean;
  menuMode: MatDrawerMode;
}

export const layoutInitialState: LayoutState = {
  isSideMenuOpen: true,
  menuMode: 'side',
};

export const layoutReducer = createReducer(
  layoutInitialState,
  on(
    LayoutActions.toggleSideMenu,
    (state): LayoutState => ({
      ...state,
      isSideMenuOpen: !state.isSideMenuOpen,
    })
  ),
  on(
    LayoutActions.setSideMenuOpened,
    (state, action): LayoutState => ({
      ...state,
      isSideMenuOpen: action.open,
    })
  ),
  on(
    LayoutActions.setMenuMode,
    (state, action): LayoutState => ({
      ...state,
      menuMode: action.mode,
    })
  ),
  on(
    LayoutActions.closeMenuOnClickLink,
    (state): LayoutState => ({
      ...state,
      isSideMenuOpen: state.menuMode === 'over' ? false : state.isSideMenuOpen,
    })
  )
);
