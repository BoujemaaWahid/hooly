import { LayoutModule } from '@angular/cdk/layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { layoutFeatureKey, layoutReducer } from './state/layout.reducer';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [ToolbarComponent, AppLayoutComponent, SideMenuComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    RouterModule,
    MatButtonModule,
    LayoutModule,
    MatDividerModule,
    StoreModule.forFeature(layoutFeatureKey, layoutReducer),
  ],
  exports: [AppLayoutComponent],
})
export class HoolyLayoutModule {}
