import { BreakpointObserver } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { LayoutActions } from '../state/layout.actions';
import { LayoutSelectors } from '../state/layout.selectors';

@Component({
  selector: 'hooly-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss'],
})
export class AppLayoutComponent implements OnInit, OnDestroy {
  isSideMenuOpen$: Observable<boolean>;
  mode$: Observable<MatDrawerMode>;
  subscriptions = new Subscription();
  constructor(
    private store: Store,
    private breakpointObserver: BreakpointObserver,
    private cdref: ChangeDetectorRef
  ) {
    this.isSideMenuOpen$ = this.store.select(
      LayoutSelectors.selectIsSideMenuOpened
    );
    this.mode$ = this.store.select(LayoutSelectors.selectMenuOpenMode);
  }

  closeSideMenu(): void {
    this.store.dispatch(LayoutActions.setSideMenuOpened({ open: false }));
  }
  breakObserver(): void {
    const max720 = '(max-width: 720px)';
    const min721 = '(min-width: 721px)';
    const observer = this.breakpointObserver
      .observe([max720, min721])
      .subscribe((result) => {
        if (result.matches) {
          const mode = result.breakpoints[max720] ? 'over' : 'side';
          this.store.dispatch(
            LayoutActions.setSideMenuOpened({
              open: !result.breakpoints[max720],
            })
          );
          this.store.dispatch(LayoutActions.setMenuMode({ mode }));
          this.cdref.detectChanges();
        }
      });
    this.subscriptions.add(observer);
  }

  ngOnInit(): void {
    this.breakObserver();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
