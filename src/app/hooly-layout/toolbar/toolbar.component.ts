import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, Observable } from 'rxjs';
import { RouterSelectors } from '../../state/router.selectors';
import { LayoutActions } from '../state/layout.actions';

@Component({
  selector: 'hooly-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
  moduleLabel$!: Observable<string>;
  constructor(private store: Store) {
    this.moduleLabel$ = this.store
      .select(RouterSelectors.selectModuleLabel)
      .pipe(filter((label) => !!label));
  }

  toggleSideMenu(): void {
    this.store.dispatch(LayoutActions.toggleSideMenu());
  }
}
