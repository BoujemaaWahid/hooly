import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LayoutActions } from '../state/layout.actions';

interface Link {
  label: string;
  url: string;
  icon: string;
}
@Component({
  selector: 'hooly-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent {
  links: Link[] = [
    {
      url: '/reservations',
      label: 'Réservations',
      icon: 'calendar.png',
    },
    { url: '/foodtrucks', label: 'FoodTrucks', icon: 'foodtruck.png' },
  ];

  constructor(private store: Store) {}

  onClickLink(): void {
    this.store.dispatch(LayoutActions.closeMenuOnClickLink());
  }
}
