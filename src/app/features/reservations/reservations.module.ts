import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../../shared/shared.module';
import { CreateReservationComponent } from './create-reservation/create-reservation.component';
import { ReservationsListComponent } from './reservations-list/reservations-list.component';
import { ReservationsRoutingModule } from './reservations-routes.module';
import { ReservationEffects } from './state/reservation.effects';
import {
  reservationFeatureKey,
  reservationReducer,
} from './state/reservation.reducer';

@NgModule({
  declarations: [ReservationsListComponent, CreateReservationComponent],
  imports: [
    CommonModule,
    ReservationsRoutingModule,
    StoreModule.forFeature(reservationFeatureKey, reservationReducer),
    EffectsModule.forFeature([ReservationEffects]),
    MatDividerModule,
    SharedModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
  ],
})
export class ReservationsModule {}
