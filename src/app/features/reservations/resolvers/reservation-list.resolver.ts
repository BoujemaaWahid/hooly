import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, tap } from 'rxjs';
import { ReservationActions } from '../state/reservation.actions';

@Injectable({
  providedIn: 'root',
})
export class ReservationListResolver implements Resolve<boolean> {
  constructor(private store: Store) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return of(true).pipe(
      tap(() => this.store.dispatch(ReservationActions.loadReservations()))
    );
  }
}
