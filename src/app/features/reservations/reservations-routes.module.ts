import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateReservationComponent } from './create-reservation/create-reservation.component';
import { ReservationsListComponent } from './reservations-list/reservations-list.component';
import { ReservationCreationResolver } from './resolvers/reservation-creation.resolver';
import { ReservationListResolver } from './resolvers/reservation-list.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: ReservationsListComponent,
    resolve: {
      loadList: ReservationListResolver,
    },
  },
  {
    path: 'create',
    resolve: {
      loadTrucks: ReservationCreationResolver,
    },
    component: CreateReservationComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationsRoutingModule {}
