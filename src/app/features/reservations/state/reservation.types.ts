export interface ReservationPage {
  page: number;
  limit: number;
}

export interface ReservationListItem {
  id: string;
  reservationDay: string;
  truckId: string;
}
export interface ReservationListDetail {
  fullSize: number;
  items: ReservationListItem[];
}

export interface ReservationTruckItem {
  id: string;
  name: string;
}

export interface Reservation {
  date: string;
  truckId: string;
}
