import { createAction, props } from '@ngrx/store';
import {
  Reservation,
  ReservationListDetail,
  ReservationTruckItem,
} from './reservation.types';

export const ReservationActions = {
  loadReservations: createAction('[Reservation] Load List'),
  loadReservationsSuccess: createAction(
    '[Reservation] Load List Success',
    props<{ detail: ReservationListDetail }>()
  ),
  loadReservationsFailure: createAction(
    '[Reservation] Load List Failure',
    props<{ error: Error }>()
  ),
  changeListPage: createAction(
    '[Reservation] Change List Page',
    props<{ page: number }>()
  ),
  loadReservationTrucks: createAction('[Reservation] Load Trucks'),
  loadReservationTrucksSuccess: createAction(
    '[Reservation] Load Trucks Success',
    props<{ trucks: ReservationTruckItem[] }>()
  ),
  loadReservationTrucksFailure: createAction(
    '[Reservation] Load Trucks Failure',
    props<{ error: Error }>()
  ),

  saveReservation: createAction(
    '[Reservation] Save Reservation',
    props<{ reservation: Reservation }>()
  ),
  saveReservationSuccess: createAction(
    '[Reservation] Save Reservation Success'
  ),
  saveReservationFailure: createAction(
    '[Reservation] Save Reservation Failure',
    props<{ error: Error }>()
  ),
};
