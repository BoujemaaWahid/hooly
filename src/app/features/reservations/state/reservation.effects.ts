import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { ReservationService } from '../service/reservation.service';
import { ReservationActions } from './reservation.actions';
import { ReservationSelectors } from './reservation.selectors';

@Injectable()
export class ReservationEffects {
  loadReservationList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        ReservationActions.loadReservations,
        ReservationActions.changeListPage
      ),
      concatLatestFrom(() =>
        this.store.select(ReservationSelectors.selectReservationPageDetail)
      ),
      switchMap(([action, params]) =>
        this.service.getReservations(params).pipe(
          map((detail) =>
            ReservationActions.loadReservationsSuccess({ detail })
          ),
          catchError((error) =>
            of(ReservationActions.loadReservationsFailure({ error }))
          )
        )
      )
    )
  );

  loadReservationTrucks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReservationActions.loadReservationTrucks),
      switchMap(() =>
        this.service.getTrucks().pipe(
          map((trucks) =>
            ReservationActions.loadReservationTrucksSuccess({ trucks })
          ),
          catchError((error) =>
            of(ReservationActions.loadReservationTrucksFailure({ error }))
          )
        )
      )
    )
  );

  saveReservation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReservationActions.saveReservation),
      switchMap((action) =>
        this.service.saveReservation(action.reservation).pipe(
          map(() => ReservationActions.saveReservationSuccess()),
          catchError((error) =>
            of(ReservationActions.saveReservationFailure({ error }))
          )
        )
      )
    )
  );

  saveReservationSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ReservationActions.saveReservationSuccess),
        tap(() => {
          this.toastr.success('', 'Réservation enregistrée', {
            positionClass: 'toast-bottom-right',
          });
          this.router.navigate(['/reservations']);
        })
      ),
    { dispatch: false }
  );

  onError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          ReservationActions.loadReservationsFailure,
          ReservationActions.loadReservationTrucksFailure,
          ReservationActions.saveReservationFailure
        ),
        tap((action) => this.toastr.error('', action.error.message))
      ),
    { dispatch: false }
  );

  constructor(
    private store: Store,
    private actions$: Actions,
    private service: ReservationService,
    private toastr: ToastrService,
    private router: Router
  ) {}
}
