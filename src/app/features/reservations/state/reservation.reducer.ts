import { createReducer, on } from '@ngrx/store';
import { ReservationActions } from './reservation.actions';
import { ReservationListItem, ReservationTruckItem } from './reservation.types';

export const reservationFeatureKey = 'reservation feature';

export interface ReservationState {
  reservations: ReservationListItem[];
  fullItemsSize: number;
  pageSize: number;
  currentPage: number;
  reservationTruckList: ReservationTruckItem[];
}

export const reservationInitialState: ReservationState = {
  reservations: [],
  fullItemsSize: 0,
  pageSize: 20,
  currentPage: 0,
  reservationTruckList: [],
};

export const reservationReducer = createReducer(
  reservationInitialState,
  on(
    ReservationActions.loadReservationsSuccess,
    (state, action): ReservationState => ({
      ...state,
      reservations: action.detail.items,
      fullItemsSize: action.detail.fullSize,
    })
  ),
  on(
    ReservationActions.changeListPage,
    (state, action): ReservationState => ({
      ...state,
      currentPage: action.page,
    })
  ),
  on(
    ReservationActions.loadReservationTrucksSuccess,
    (state, action): ReservationState => ({
      ...state,
      reservationTruckList: action.trucks,
    })
  )
);
