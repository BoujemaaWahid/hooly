import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TablePaginationInfos } from '../../../shared/table/table.types';
import { reservationFeatureKey, ReservationState } from './reservation.reducer';
import {
  ReservationListItem,
  ReservationPage,
  ReservationTruckItem,
} from './reservation.types';

const selectState = createFeatureSelector<ReservationState>(
  reservationFeatureKey
);
const selectReservationList = createSelector(
  selectState,
  (state): ReservationListItem[] => state.reservations
);
const selectCurrentPage = createSelector(
  selectState,
  (state): number => state.currentPage
);
const selectFullSize = createSelector(
  selectState,
  (state): number => state.fullItemsSize
);

const selectPageSize = createSelector(
  selectState,
  (state): number => state.pageSize
);
const selectReservationPageDetail = createSelector(
  selectCurrentPage,
  selectPageSize,
  (page, limit): ReservationPage => ({ page, limit })
);
const selectReservationTruckList = createSelector(
  selectState,
  (state): ReservationTruckItem[] => state.reservationTruckList
);

const selectPaginationInfos = createSelector(
  selectReservationPageDetail,
  selectFullSize,
  (detail, fullSize): TablePaginationInfos => ({
    currentPage: detail.page,
    fullSize,
    pageSize: detail.limit,
  })
);
export const ReservationSelectors = {
  selectState,
  selectReservationList,
  selectReservationPageDetail,
  selectReservationTruckList,
  selectPaginationInfos,
};
