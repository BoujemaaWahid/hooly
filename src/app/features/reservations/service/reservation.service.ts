import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, mergeMap, Observable } from 'rxjs';
import { AppSelectors } from '../../../state/app.selectors';
import {
  Reservation,
  ReservationListDetail,
  ReservationPage,
  ReservationTruckItem,
} from '../state/reservation.types';
@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  constructor(private http: HttpClient, private store: Store) {}
  getReservations(params: ReservationPage): Observable<ReservationListDetail> {
    return this.store.select(AppSelectors.selectApiUrl).pipe(
      filter((url) => !!url),
      mergeMap((url) =>
        this.http.get<ReservationListDetail>(
          `${url}/reservations-page/${params.page}/${params.limit}`
        )
      )
    );
  }

  getTrucks(): Observable<ReservationTruckItem[]> {
    return this.store.select(AppSelectors.selectApiUrl).pipe(
      filter((url) => !!url),
      mergeMap((url) =>
        this.http.get<ReservationTruckItem[]>(`${url}/trucks-names`)
      )
    );
  }

  saveReservation(reservation: Reservation) {
    return this.store.select(AppSelectors.selectApiUrl).pipe(
      filter((url) => !!url),
      mergeMap((url) => this.http.post(`${url}/reservation-save`, reservation))
    );
  }
}
