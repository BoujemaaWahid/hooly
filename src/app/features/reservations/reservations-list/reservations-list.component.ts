import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  TableColumn,
  TablePaginationInfos,
} from '../../../shared/table/table.types';
import { ReservationSelectors } from '../state/reservation.selectors';
import { ReservationListItem } from '../state/reservation.types';
import { ReservationActions } from './../state/reservation.actions';

@Component({
  selector: 'hooly-reservations-list',
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.scss'],
})
export class ReservationsListComponent {
  displayedColumns: TableColumn<ReservationListItem>[] = [
    { key: 'id', label: 'N° réservation', type: 'string' },
    { key: 'reservationDay', label: 'Jour de réservation', type: 'date' },
    { key: 'truckId', label: 'N° truck', type: 'string' },
  ];

  reservations$: Observable<ReservationListItem[]>;
  paginationInfos$: Observable<TablePaginationInfos>;

  constructor(private store: Store) {
    this.reservations$ = this.store.select(
      ReservationSelectors.selectReservationList
    );
    this.paginationInfos$ = this.store.select(
      ReservationSelectors.selectPaginationInfos
    );
  }
  onChangePage(page: number): void {
    this.store.dispatch(ReservationActions.changeListPage({ page }));
  }
}
