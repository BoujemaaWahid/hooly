import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ReservationActions } from '../state/reservation.actions';
import { Reservation, ReservationTruckItem } from '../state/reservation.types';
import { ReservationSelectors } from './../state/reservation.selectors';

@Component({
  selector: 'hooly-create-reservation',
  templateUrl: './create-reservation.component.html',
  styleUrls: ['./create-reservation.component.scss'],
})
export class CreateReservationComponent {
  reservationForm = new FormGroup({
    reservationDay: new FormControl('', Validators.required),
    truckId: new FormControl('', Validators.required),
  });

  trucks$: Observable<ReservationTruckItem[]>;
  constructor(private store: Store) {
    this.trucks$ = this.store.select(
      ReservationSelectors.selectReservationTruckList
    );
  }

  saveReservation(): void {
    this.store.dispatch(
      ReservationActions.saveReservation({
        reservation: this.reservationForm.value as Reservation,
      })
    );
  }
}
