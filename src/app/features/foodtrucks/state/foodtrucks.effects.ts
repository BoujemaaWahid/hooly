import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { FoodTrucksService } from '../service/foodtrucks.service';
import { FoodTrucksActions } from './foodtrucks.actions';
import { FoodTrucksSelectors } from './foodtrucks.selector';

@Injectable()
export class FoodTrucksEffects {
  loadFoodTrucks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        FoodTrucksActions.loadFoodTrucks,
        FoodTrucksActions.changeListPage
      ),
      concatLatestFrom(() =>
        this.store.select(FoodTrucksSelectors.selectReservationPageDetail)
      ),
      switchMap(([action, params]) =>
        this.service.getFoodTrucks(params).pipe(
          map((detail) => FoodTrucksActions.loadFoodTrucksSuccess({ detail })),
          catchError((error) =>
            of(FoodTrucksActions.loadFoodTrucksFailure({ error }))
          )
        )
      )
    )
  );

  saveFoodTruck$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FoodTrucksActions.saveFoodTruck),
      switchMap((action) =>
        this.service.saveFoodTruck(action.truck).pipe(
          map((trucks) => FoodTrucksActions.saveFoodTruckSuccess()),
          catchError((error) =>
            of(FoodTrucksActions.saveFoodTruckFailure({ error }))
          )
        )
      )
    )
  );

  saveFoodTruckSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(FoodTrucksActions.saveFoodTruckSuccess),
        tap(() => {
          this.toastr.success('', 'Food truck enregistrée', {
            positionClass: 'toast-bottom-right',
          });
          this.router.navigate(['/foodtrucks']);
        })
      ),
    { dispatch: false }
  );

  onError$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          FoodTrucksActions.loadFoodTrucksFailure,
          FoodTrucksActions.saveFoodTruckFailure
        ),
        tap((action) =>
          this.toastr.error('Error', action.error.message, { timeOut: 1200 })
        )
      ),
    { dispatch: false }
  );
  constructor(
    private store: Store,
    private actions$: Actions,
    private service: FoodTrucksService,
    private toastr: ToastrService,
    private router: Router
  ) {}
}
