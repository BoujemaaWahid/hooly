import { createAction, props } from '@ngrx/store';
import { FoodTruckName, FoodTrucksListDetail } from './foodtrucks.types';

export const FoodTrucksActions = {
  loadFoodTrucks: createAction('[FoodTrucks] Load foodtrucks'),
  loadFoodTrucksSuccess: createAction(
    '[FoodTrucks] Load foodtrucks success',
    props<{ detail: FoodTrucksListDetail }>()
  ),
  loadFoodTrucksFailure: createAction(
    '[FoodTrucks] Load foodtrucks failure',
    props<{ error: Error }>()
  ),
  changeListPage: createAction(
    '[FoodTrucks] Change List Page',
    props<{ page: number }>()
  ),
  saveFoodTruck: createAction(
    '[FoodTrucks] Save foodtruck',
    props<{ truck: FoodTruckName }>()
  ),
  saveFoodTruckSuccess: createAction('[FoodTrucks] Save foodtruck success'),
  saveFoodTruckFailure: createAction(
    '[FoodTrucks] Save foodtruck failure',
    props<{ error: Error }>()
  ),
};
