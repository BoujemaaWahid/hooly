import { createReducer, on } from '@ngrx/store';
import { FoodTrucksActions } from './foodtrucks.actions';
import { FoodTruckListItem } from './foodtrucks.types';

export const foodTrucksFeatureKey = 'foodTrucks';
export interface FoodTrucksState {
  fullItemsSize: number;
  pageSize: number;
  currentPage: number;
  foodTrucks: FoodTruckListItem[];
}
export const initialState: FoodTrucksState = {
  fullItemsSize: 0,
  pageSize: 15,
  currentPage: 0,
  foodTrucks: [],
};

export const foodTrucksReducer = createReducer(
  initialState,
  on(
    FoodTrucksActions.loadFoodTrucksSuccess,
    (state, action): FoodTrucksState => ({
      ...state,
      foodTrucks: action.detail.items,
      fullItemsSize: action.detail.fullSize,
    })
  ),

  on(
    FoodTrucksActions.changeListPage,
    (state, action): FoodTrucksState => ({
      ...state,
      currentPage: action.page,
    })
  )
);
