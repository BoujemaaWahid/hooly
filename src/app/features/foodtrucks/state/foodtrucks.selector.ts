import { createFeatureSelector, createSelector } from '@ngrx/store';
import { TablePaginationInfos } from '../../../shared/table/table.types';
import { foodTrucksFeatureKey, FoodTrucksState } from './foodtrucks.reducer';
import { FoodTruckListItem, FoodTrucksPage } from './foodtrucks.types';

const selectState =
  createFeatureSelector<FoodTrucksState>(foodTrucksFeatureKey);

const selectCurrentPage = createSelector(
  selectState,
  (state): number => state.currentPage
);
const selectFullSize = createSelector(
  selectState,
  (state): number => state.fullItemsSize
);

const selectPageSize = createSelector(
  selectState,
  (state): number => state.pageSize
);

const selectReservationPageDetail = createSelector(
  selectPageSize,
  selectCurrentPage,
  (limit, page): FoodTrucksPage => ({ page, limit })
);

const selectFoodTrucksList = createSelector(
  selectState,
  (state): FoodTruckListItem[] => state.foodTrucks
);

const selectPaginationInfos = createSelector(
  selectReservationPageDetail,
  selectFullSize,
  (detail, fullSize): TablePaginationInfos => ({
    currentPage: detail.page,
    fullSize,
    pageSize: detail.limit,
  })
);

export const FoodTrucksSelectors = {
  selectState,
  selectCurrentPage,
  selectFullSize,
  selectReservationPageDetail,
  selectPageSize,
  selectFoodTrucksList,
  selectPaginationInfos,
};
