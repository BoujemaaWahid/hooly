export interface FoodTruckListItem {
  id: string;
  name: string;
}

export interface FoodTruckName {
  name: string;
}

export interface FoodTrucksListDetail {
  fullSize: number;
  items: FoodTruckListItem[];
}
export interface FoodTrucksPage {
  page: number;
  limit: number;
}
