import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of, tap } from 'rxjs';
import { FoodTrucksActions } from '../state/foodtrucks.actions';

@Injectable({
  providedIn: 'root',
})
export class FoodTrucksListResolver implements Resolve<boolean> {
  constructor(private store: Store) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return of(true).pipe(
      tap(() => this.store.dispatch(FoodTrucksActions.loadFoodTrucks()))
    );
  }
}
