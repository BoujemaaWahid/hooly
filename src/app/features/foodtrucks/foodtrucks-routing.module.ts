import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateFoodtruckComponent } from './create-foodtruck/create-foodtruck.component';
import { FoodtrucksListComponent } from './foodtrucks-list/foodtrucks-list.component';
import { FoodTrucksListResolver } from './resolvers/foodtrucks-list.resolver';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: 'list',
    component: FoodtrucksListComponent,
    resolve: { foodtrucks: FoodTrucksListResolver },
  },
  { path: 'create', component: CreateFoodtruckComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodTrucksRoutingModule {}
