import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, mergeMap, Observable } from 'rxjs';
import { AppSelectors } from '../../../state/app.selectors';
import {
  FoodTruckName,
  FoodTrucksListDetail,
  FoodTrucksPage,
} from '../state/foodtrucks.types';

@Injectable({
  providedIn: 'root',
})
export class FoodTrucksService {
  constructor(private http: HttpClient, private store: Store) {}

  getFoodTrucks(params: FoodTrucksPage): Observable<FoodTrucksListDetail> {
    return this.store.select(AppSelectors.selectApiUrl).pipe(
      filter((url) => !!url),
      mergeMap((url) =>
        this.http.get<FoodTrucksListDetail>(
          `${url}/foodtrucks-page/${params.page}/${params.limit}`
        )
      )
    );
  }

  saveFoodTruck(foodTruck: FoodTruckName) {
    return this.store.select(AppSelectors.selectApiUrl).pipe(
      filter((url) => !!url),
      mergeMap((url) => this.http.post(`${url}/foodtrucks-save`, foodTruck))
    );
  }
}
