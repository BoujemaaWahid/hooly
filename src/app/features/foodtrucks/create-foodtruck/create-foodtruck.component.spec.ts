import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFoodtruckComponent } from './create-foodtruck.component';

describe('CreateFoodtruckComponent', () => {
  let component: CreateFoodtruckComponent;
  let fixture: ComponentFixture<CreateFoodtruckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFoodtruckComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFoodtruckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
