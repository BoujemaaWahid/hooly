import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { FoodTrucksActions } from '../state/foodtrucks.actions';

@Component({
  selector: 'hooly-create-foodtruck',
  templateUrl: './create-foodtruck.component.html',
  styleUrls: ['./create-foodtruck.component.scss'],
})
export class CreateFoodtruckComponent {
  foodTruckForm = new FormGroup({
    name: new FormControl('', Validators.required),
  });
  constructor(private store: Store) {}

  saveFoodTruck(): void {
    this.store.dispatch(
      FoodTrucksActions.saveFoodTruck({ truck: this.foodTruckForm.value })
    );
  }
}
