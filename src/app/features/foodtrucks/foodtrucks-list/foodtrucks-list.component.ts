import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  TableColumn,
  TablePaginationInfos,
} from '../../../shared/table/table.types';
import { FoodTrucksActions } from '../state/foodtrucks.actions';
import { FoodTrucksSelectors } from '../state/foodtrucks.selector';
import { FoodTruckListItem } from '../state/foodtrucks.types';

@Component({
  selector: 'hooly-foodtrucks-list',
  templateUrl: './foodtrucks-list.component.html',
  styleUrls: ['./foodtrucks-list.component.scss'],
})
export class FoodtrucksListComponent {
  displayedColumns: TableColumn<FoodTruckListItem>[] = [
    { key: 'id', label: 'ID', type: 'string' },
    { key: 'name', label: 'Label', type: 'string' },
  ];

  foodTrucks$: Observable<FoodTruckListItem[]>;
  paginationInfos$: Observable<TablePaginationInfos>;

  constructor(private store: Store) {
    this.foodTrucks$ = this.store.select(
      FoodTrucksSelectors.selectFoodTrucksList
    );
    this.paginationInfos$ = this.store.select(
      FoodTrucksSelectors.selectPaginationInfos
    );
  }

  onChangePage(page: number): void {
    this.store.dispatch(FoodTrucksActions.changeListPage({ page }));
  }
}
