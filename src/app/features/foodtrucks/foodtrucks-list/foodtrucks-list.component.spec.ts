import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodtrucksListComponent } from './foodtrucks-list.component';

describe('FoodtrucksListComponent', () => {
  let component: FoodtrucksListComponent;
  let fixture: ComponentFixture<FoodtrucksListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodtrucksListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoodtrucksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
