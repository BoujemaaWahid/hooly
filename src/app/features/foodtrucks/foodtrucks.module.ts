import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from './../../shared/shared.module';
import { CreateFoodtruckComponent } from './create-foodtruck/create-foodtruck.component';
import { FoodtrucksListComponent } from './foodtrucks-list/foodtrucks-list.component';
import { FoodTrucksRoutingModule } from './foodtrucks-routing.module';
import { FoodTrucksEffects } from './state/foodtrucks.effects';
import {
  foodTrucksFeatureKey,
  foodTrucksReducer,
} from './state/foodtrucks.reducer';

@NgModule({
  declarations: [FoodtrucksListComponent, CreateFoodtruckComponent],
  imports: [
    CommonModule,
    FoodTrucksRoutingModule,
    MatButtonModule,
    StoreModule.forFeature(foodTrucksFeatureKey, foodTrucksReducer),
    EffectsModule.forFeature([FoodTrucksEffects]),
    SharedModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class FoodtrucksModule {}
