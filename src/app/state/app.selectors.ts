import { createSelector } from '@ngrx/store';
import { environment } from '../../environments/environment';

const selectApiUrl = createSelector((): string => environment.apiUrl);

export const AppSelectors = {
  selectApiUrl,
};
