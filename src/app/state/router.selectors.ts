import { NavigationExtras, Params } from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import { getSelectors } from '@ngrx/router-store';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface RouterState {
  url: string;
  params: Params;
  queryParams: Params;
  extras: NavigationExtras;
}

export const selectRouter =
  createFeatureSelector<fromRouter.RouterReducerState<RouterState>>('router');

const {
  selectCurrentRoute,
  selectFragment,
  selectQueryParams,
  selectQueryParam,
  selectRouteParams,
  selectRouteParam,
  selectRouteData,
  selectUrl,
} = getSelectors(selectRouter);

const selectModuleLabel = createSelector(selectRouteData, (data): string =>
  !!data ? data['moduleLabel'] : ''
);
const selectUrlAsArray = createSelector(selectUrl, (url): string[] =>
  url.split('/')
);
export const RouterSelectors = {
  selectUrl,
  selectRouteData,
  selectRouteParams,
  selectCurrentRoute,
  selectUrlAsArray,
  selectModuleLabel,
};
