import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'reservations',
    pathMatch: 'full',
  },
  {
    path: 'reservations',
    data: { moduleLabel: 'Réservations' },
    loadChildren: () =>
      import('./features/reservations/reservations.module').then(
        (m) => m.ReservationsModule
      ),
  },
  {
    path: 'foodtrucks',
    data: { moduleLabel: 'Food trucks' },
    loadChildren: () =>
      import('./features/foodtrucks/foodtrucks.module').then(
        (m) => m.FoodtrucksModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: 'enabled',
      paramsInheritanceStrategy: 'always',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
