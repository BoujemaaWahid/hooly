export interface TableColumn<T> {
  key: keyof T;
  label: string;
  type: 'date' | 'string';
}
export interface TablePaginationInfos {
  currentPage: number;
  fullSize: number;
  pageSize: number;
}
