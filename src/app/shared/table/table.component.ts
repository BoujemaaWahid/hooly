import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Observable, of } from 'rxjs';
import { TableColumn, TablePaginationInfos } from './table.types';

@Component({
  selector: 'hooly-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableComponent {
  @Input() dataSource: Observable<any> = of([]);
  @Input() columns: TableColumn<any>[] = [];
  @Input() tableLabel: string = '';
  @Input() paginationInfos: TablePaginationInfos | null = null;
  @Output() changePage = new EventEmitter<number>();

  onChangePage(page: PageEvent): void {
    this.changePage.emit(page.pageIndex);
  }

  get displayedColumnsList(): string[] {
    return this.columns?.map((column) => column.key.toString()) ?? [];
  }
}
